import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { Router} from "@angular/router";
/*---Plugins---*/
import {Storage} from "@ionic/storage";

/*-- Services---*/
import {UsuarioService} from "../../providers/usuario.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    email:string = "";
    pass:string = "";
    token:any;
    remember:boolean = false;
    userData:any;
  constructor(public http: HttpClient, private storage: Storage,  private router: Router, private _us:UsuarioService) {
      this.storage.get('email').then((val) => {
          this.email = val;
      });
      this.storage.get('pass').then((val) => {
          this.pass = val;
      });
  }

  ngOnInit() {
  }

    sendRegister(){
       this.router.navigate(['/register']);
    }


    login(){
        this._us.login(this.email,this.pass)
            .subscribe((data)=>{
                    /*   this.alertCtrl.create({
                         title:"Logeado",
                         //subTitle:data,
                         buttons:["OK"]
                       }).present();*/
                    console.log('logeado');
                    if(this.remember == true){
                        this.storage.set('email', this.email);
                        this.storage.set('pass', this.pass);
                    }else{
                        this.remember = false;
                    }
                    this.storage.get('introShown').then((result)=>{
                        console.log(result);
                        if(result  == true){
                            this.router.navigateByUrl('/tabs');
                        }else{
                            this.router.navigateByUrl('/tabs');
                            this.storage.set('introShown', true);
                        }
                    });
                },
                (err) => {
                    /*this.altrCtrl.create({
                        title:"Error",
                        subTitle:"Verifica tu correo y contraseña",
                        buttons:["OK"]
                    }).present();*/
                    console.log('not login');
                }
            )
    }
}
