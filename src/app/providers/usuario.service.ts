import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {map} from "rxjs/internal/operators";
import {URL_SERVICES, URL_LOGIN} from "../../app/config/url.services";
import {Storage} from "@ionic/storage";
import {Platform} from "@ionic/angular";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
    token:string;
    identity:any;
    id_usuario:string;
    email: any;
    public loginState : boolean = false;
  constructor(
      public http: HttpClient,
      private platform:Platform,
      private storage: Storage,
      private router:Router
  ) { }

  login(email:string , pass:string){
      let data = new HttpParams();
      data = data.append("phone", email);
      data = data.append("nip", pass);
      let headers = new HttpHeaders();
      headers= headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers= headers.append('Accept', 'application/json');
      headers= headers.append('Access-Control-Allow-Origin', 'http://localhost/8100');
      headers= headers.append('Access-Control-Allow-Methods', 'POST,GET,PUT,OPTIONS');
      headers= headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
      headers= headers.append('Access-Control-Allow-Credentials', 'true');
      console.log(data);
      return this.http.post(URL_SERVICES+"/api/customer/login",data,{headers:headers})
          .pipe(map(( resp: Response) =>{
              let data_resp = resp;
              console.log(data_resp);
              //console.log(data_resp.error.mensaje);
              if(data_resp){
                  this.identity = data_resp;
                  this.token = this.identity.data.access_token;
                  this.id_usuario = this.identity.data.customer.id;
                  this.loginState = true;
                  this.storage.set('loginState', this.loginState);
                  this.storage.set('token', this.token);
                  this.storage.set('id', this.id_usuario);

              }else{
                  /**/
                  this.identity = '';
                  this.token = '';
                  this.id_usuario = '';
                  //Guardar storage
              }

          }));
  }
    logout(){
        this.storage.get('token').then((data) => {
            let url = URL_SERVICES + "/api/customer/logout";
            let headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers= headers.append('Authorization', 'Bearer '+data);
            this.http.post(url,null,{headers:headers}).subscribe(response => {
                console.log(response);
                this.storage.remove('loginState').then(value => {
                    this.router.navigateByUrl('/login');
                });
            });
        });
    }
}
