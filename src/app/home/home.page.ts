import { Component} from '@angular/core';
import {NavController, PopoverController} from "@ionic/angular";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {URL_SERVICES} from "../../app/config/url.services";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    items:any = [];
    searchTerm: string ;
    points:any;
    test:any=[];
    constructor(public navCtrl: NavController,public popoverCtrl: PopoverController, public http: HttpClient, private storage: Storage) {
        //this.getPackages();
        this.initialItems();
        this.getPoints();
    }
    initialItems(){
        this.items = [
            {
                puc:"PUC ATIZAPAN",
                id:"1",
                paq:"Estafeta S.A de C.V",
                rem: "Juan Perez",
                clave: "123451",
                alias:"Zona Esmeralda",
                address:"atizapan nuevo mexico 12 mexico",
                state:"Buen estado",
                status:"Click&collect",
                delivery:"PIDE UN ACCESS",
                id_status:2,
                hour:"12:30 pm",
                date:"12-12-2018",
                price:"200",
                finish:"17-12-2018"
            },
            {
                puc:"PUC NAUCALPAN",
                id:"2",
                paq:"Estafeta S.A de C.V",
                rem: "Pedro Perez",
                clave: "123451",
                alias:"Naucalpan",
                address:"atizapan nuevo mexico 12 mexico",
                state:"Buen estado",
                status:"Almacenado",
                delivery:"PIDE UN ACCESS",
                id_status:1,
                hour:"12:30 pm",
                date:"12-12-2018",
                finish:"17-12-2018",
                price:"100"
            },
            {
                puc:"PUC TLALNE",
                id:"3",
                paq:"Estafeta S.A de C.V",
                rem: "Rocio Dorial",
                clave: "123451",
                delivery:"PIDE UN ACCESS",
                alias:"Tlalne",
                address:"atizapan nuevo mexico 12 mexico",
                state:"Buen estado",
                status:"Click&collect",
                id_status:2,
                hour:"12:30 pm",
                date:"12-12-2018",
                price:"300"
            },
            {
                puc:"PUC TLALNE",
                id:"4",
                paq:"Estafeta S.A de C.V",
                rem: "Roto Podrian",
                clave: "123451",
                alias:"Tlalne",
                delivery:"PIDE UN ACCESS",
                address:"atizapan nuevo mexico 12 mexico",
                state:"Mal estado",
                status:"Almacenado",
                id_status:1,
                hour:"12:30 pm",
                date:"12-12-2018",
                finish:"17-12-2018",
                price:"150"
            },
        ];
        this.test = {
            puc:"PUC TLALNE",
            id:"5",
            rem: "Roto Podrian",
            clave: "123451",
            alias:"Tlalne",
            address:"atizapan nuevo mexico 12 mexico",
            state:"Mal estado",
            status:"Click&collect",
            date:"12-12-2018",
            finish:"17-12-2018",
            hour:"12:30 pm",
            price:"150"
        };

        console.log(URL_SERVICES);
    }
    getPoints(){
        this.storage.get('token').then((token) => {
            this.storage.get('id').then((data) => {
                console.log(data);
                let url = URL_SERVICES + "/api/customer/customer_points";
                let headers = new HttpHeaders();
                let js = JSON.stringify({
                    "customer":data
                });
                headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers = headers.append('Content-Type', 'application/json');
                headers= headers.append('Authorization', 'Bearer '+ token);
                this.http.post(url,js,{headers:headers}).subscribe(response => {
                    this.points = response;
                    console.log(response);
                    //this.items = response;
                });
            });
        });
    }
}
