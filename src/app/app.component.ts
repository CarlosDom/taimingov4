import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from "@ionic/storage";
import {timer} from "rxjs/index";
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
        this.storage.get('loginState').then((value)=>{
            console.log(value);
            this.splashScreen.hide();
            timer(5000).subscribe(()=>{this.ShowSplash = false});
            if(value == true) {
                //Comprobar si es la primera vez que abre la app
                this.router.navigateByUrl('/tabs');
                this.splashScreen.hide();
                this.statusBar.styleLightContent();
            }else{
                this.router.navigateByUrl('/login');
                this.splashScreen.hide();
                this.statusBar.styleLightContent();
            }
        }).catch(()=>{
            this.router.navigateByUrl('/login');
            this.splashScreen.hide();
            this.statusBar.styleLightContent();
        });

        this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
